﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Shows an inviduals highscore;
/// </summary>
public class FrontendHighscoreListElement : MonoBehaviour
{
    public TextMeshProUGUI textName;
    public TextMeshProUGUI textScore;

    public void SetInfo(string name, string score)
    {
        textName.text = name;
        textScore.text = score;
    }
}
