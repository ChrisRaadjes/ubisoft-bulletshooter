﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour
{
    public float spawnRateMin = 10;
    public float spawnRateMax = 20;
    public float spawnPositionOffset = 1.1f;
    public int initialSpawn = 3;

    float nextSpawnTick;
    float timeSinceLastSpawn;

    bool initialized = false;
    bool inGameplay = false;

    public void Initialise()
    {
        GameController.Instance.onGameStateChanged.AddListener(
            (currentGameState) => GameplayStateChange(currentGameState));

        initialized = true;
    }

    public void GameplayStateChange(GameController.GameState currentGameState)
    {
        inGameplay = currentGameState == GameController.GameState.Gameplay;

        if(inGameplay)
        {
            Invoke("InitialSpawn", 2f);
        }
    }

    private void InitialSpawn()
    {
        for (int i = 0; i < initialSpawn; ++i)
        {
            SpawnPickup();
        }
    }

    void Update()
    {
        if (inGameplay)
        {
            timeSinceLastSpawn += Time.deltaTime;

            if (timeSinceLastSpawn >= nextSpawnTick)
            {
                SpawnPickup();
            }
        }
    }

    private void SpawnPickup()
    {
        SetNewSpawnTime();

        // Get enemy and set position:
        var goPickup = ObjectPooler.Instance.GetPooledObject("Pickup");

        //Get the camera boundss
        var bounds = GameController.Instance.GetCameraBounds();

        var pickup = goPickup.GetComponent<Pickup>();
        var rendererBounds = pickup.RendererBounds;
        float rendererSize = rendererBounds.size.y * 0.5f;

        //Set top and bottom corners modified by the size of the enemy render
        float yMin = bounds.min.y + rendererSize;
        float yMax = bounds.max.y - rendererSize;

        // Get a random yPos
        float y = Random.Range(yMin, yMax);

        // Spawn just off screen
        float x = bounds.max.x * spawnPositionOffset;

        pickup.SpawnEntity(new Vector2(x, y));
    }

    void SetNewSpawnTime()
    {
        // Make sure the timer get reset first
        timeSinceLastSpawn = 0f;

        nextSpawnTick = Random.Range(
            spawnRateMin,
            spawnRateMax);
    }
}
