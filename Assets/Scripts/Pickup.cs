﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    [SerializeField] private float movementSpeed = 4f;
    [SerializeField] private int ammoCount = 50;

    public int AmmoCount
    {
        get { return ammoCount; }
    }

    [SerializeField] private Renderer renderer;
    [SerializeField] private Collider2D collider;
    [SerializeField] private LayerMask collisionMask;

    public Bounds RendererBounds
    {
        get { return renderer.bounds; }
    }

    public void SpawnEntity(Vector3 worldPos)
    {
        gameObject.SetActive(true);
        MoveToLocation(worldPos);
        SetColliderIsTrigger(true);
    }

    void Update()
    {
        Vector3 direction = Vector3.left;
        direction.Normalize();

        transform.position += direction * movementSpeed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(collisionMask == (collisionMask | (1 << other.gameObject.layer)))
        {
            // The playerpawn takes care of adding the ammo.
            Destroy();
            return;
        }
    }

    private void Destroy()
    {
        gameObject.SetActive(false);
        SetColliderIsTrigger(false);
    }

    public void MoveToLocation(Vector3 worldPos)
    {
        transform.position = worldPos;
    }

    void SetColliderIsTrigger(bool value)
    {
        collider.isTrigger = value;
    }
}
