﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Weapon Class
/// </summary>
public class Weapon : MonoBehaviour {

    [SerializeField] private Transform[] turrets;
    [SerializeField] private float fireRate;
    [SerializeField] private string bulletTag; // I really dislike this method of pooling.

    [SerializeField] private bool infiniteAmmo = true;
    [SerializeField] private int startingAmmo = 10;
    [SerializeField] private int currentAmmoCount = 500;

    bool canFireWeapon = true;
    float timeSinceLastShot;

    /// <summary>
    /// Attempts to fire a shot if the time since last shot is smaller than the fire rate,
    /// and the player has ammo remaining.
    /// </summary>
  
    public void TryFireWeapon()
    {
        if(canFireWeapon)
        {
            SetFireDelay();

            // Fire once for each turret
            for (int t = 0; t < turrets.Length; ++t)
            {
                Transform turret = turrets[t];

                // Need to use the bulletType later. 
                // Check ammo. 
                if (currentAmmoCount > 0)
                {
                    var goBullet = ObjectPooler.Instance.GetPooledObject(bulletTag);
                    if (goBullet != null)
                    {
                        //Pretty sure this is wrong, need to use Quaternion.Euler to get and 
                        // set the directional vector. 

                        var bullet = goBullet.GetComponent<Bullet>();

                        Vector3 forwardVector = turret.transform.rotation * Vector3.forward;
                        bullet.direction = forwardVector;       
                        goBullet.transform.position = turret.position;

                        //goBullet.transform.rotation = turret.rotation;

                        goBullet.SetActive(true);

                        // Decrease ammo
                        UpdateAmmo(-1);
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    public class OnAmmoChanged : UnityEvent<int>
    {
    }

    public OnAmmoChanged onAmmoChanged = new OnAmmoChanged();

    public void ResetAmmo()
    {
        if (infiniteAmmo)
            return;

        currentAmmoCount = startingAmmo;
        onAmmoChanged.Invoke(currentAmmoCount);
    }

    public void UpdateAmmo(int value)
    {
        if (!infiniteAmmo)
            currentAmmoCount += value;

        onAmmoChanged.Invoke(currentAmmoCount);
    }

    private void SetFireDelay()
    {
        canFireWeapon = false;
        timeSinceLastShot = 0f;
    }

    private void Update()
    {
        if(!canFireWeapon)
            timeSinceLastShot += Time.deltaTime;

        if (timeSinceLastShot >= fireRate)
            canFireWeapon = true;
    }
}
