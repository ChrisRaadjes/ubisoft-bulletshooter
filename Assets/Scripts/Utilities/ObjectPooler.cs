﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// I'd use the Auroch generic monobehaviour pooler since it's much better; but got to roll my own.
/// </summary>
/// 
public class ObjectPooler : MonoBehaviour {

    private static ObjectPooler instance;

    public List<ObjectPoolItem> itemsToPool;
    public List<GameObject> pooledObjects;

    public static ObjectPooler Instance
    {
        get { return instance; }
    }

    [System.Serializable]
    public class ObjectPoolItem
    {
        public int amount;
        public GameObject objectToPool;

        // This is to let UGUI object stay under a dedicated rectTransform, 
        // circumventing a lot of headache.
        public Transform optionalPoolLocation; 

        public bool canGrow = true;
    }


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        pooledObjects = new List<GameObject>();

        foreach(ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.amount; ++i)
            {
                GameObject go = (GameObject)Instantiate(item.objectToPool);

                // If an optional pooler location has been assigned...
                if (item.optionalPoolLocation != null)
                    go.transform.SetParent(item.optionalPoolLocation);

                go.SetActive(false);
                pooledObjects.Add(go);
            }

        }
    }

    public GameObject GetPooledObject(string tag)
    {
        for(int i = 0; i < pooledObjects.Count; ++i)
        {
            if(!pooledObjects[i].activeInHierarchy && pooledObjects[i].tag == tag)
            {
                return pooledObjects[i];
            }
        }

        // Grow pool if no objects are avaiable
        foreach(ObjectPoolItem item in itemsToPool)
        {
            if(item.objectToPool.tag == tag)
            {
                if (item.canGrow)
                {
                    GameObject obj = (GameObject)Instantiate(item.objectToPool);
                    obj.SetActive(false);
                    pooledObjects.Add(obj);
                    return obj;
                }
            }
            else
            {
                return null;
            }
        }

        return null;
    }





}
