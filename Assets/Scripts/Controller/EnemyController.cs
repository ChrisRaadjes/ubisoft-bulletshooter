﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// For spawning and keeping track of enemies.
/// </summary>
public class EnemyController : MonoBehaviour {

    public float enemySpawnRateMaxStart = 5f;
    float enemySpawnRateMaxCurrent;

    public float enemySpawnRateMin = 2f;
    public float spawnRateMultiplier = 1f;
    public float spawnPositionOffset = 1.1f;
    public float randomXOffsetMin = 1f;
    public float randomXOffsetMax = 3f;

    [Header("Spawn Threshold Base")]
    public int spawnRateBase = 1;

    [Header("Spawn Threshold Low")]
    public int spawnRateLow = 2;
    public float spawnRateThresholdLow = 1f;

    [Header("Spawn Threshold Medium")]
    public int spawnRateMedium = 4;
    public float spawnRateThresholdMedium = 5f;

    [Header("Spawn Threshold Max")]
    public int spawnRateHigh = 6;
    public float spawnRateThresholdHigh = 10f;

    float elapsedTime = 0f;
    int currentSpawnRate = 1;

    float nextSpawnTick;
    float timeSinceLastSpawn;

    bool initialized = false;

    bool inGameplay = false;
    
    public void Initialise()
    {
        GameController.Instance.onGameStateChanged.AddListener(
            (currentGameState) => GameplayStateChange(currentGameState));

        initialized = true;
    }

    public void GameplayStateChange(GameController.GameState currentGameState)
    {
        inGameplay = currentGameState == GameController.GameState.Gameplay;
        
        //Technically only need once, but doesn't make a difference in this instance.
        Reset();
    }

    private void Reset()
    {
        enemySpawnRateMaxCurrent = enemySpawnRateMaxStart;
        timeSinceLastSpawn = 0f;
        elapsedTime = 0f;
    }

    void Update ()
    {
        if(inGameplay)
        {
            if (!initialized)
                return;

            timeSinceLastSpawn += Time.deltaTime;
            elapsedTime += Time.deltaTime;

            SetSpawnNumber();

            if (timeSinceLastSpawn >= nextSpawnTick)
            {
                SpawnEnemy(currentSpawnRate);
            }

            float newMaxSpawnRate = enemySpawnRateMaxStart -
                (Time.fixedDeltaTime * spawnRateMultiplier);

            enemySpawnRateMaxCurrent = Mathf.Clamp(
                newMaxSpawnRate,
                enemySpawnRateMin,
                enemySpawnRateMaxStart);
        }
	}

    void SetSpawnNumber()
    {
        if (elapsedTime >= spawnRateThresholdHigh)
            currentSpawnRate = spawnRateHigh;
        else if (elapsedTime >= spawnRateThresholdMedium)
            currentSpawnRate = spawnRateMedium;
        else if (elapsedTime >= spawnRateThresholdLow)
            currentSpawnRate = spawnRateLow;
        else
            currentSpawnRate = spawnRateBase;
    }

    void SetNewSpawnTime()
    {
        // Make sure the timer get reset first
        timeSinceLastSpawn = 0f;

        nextSpawnTick = Random.Range(
            enemySpawnRateMin, 
            enemySpawnRateMaxCurrent);
    }

    void SpawnEnemy(int amount)
    {
        SetNewSpawnTime();

        for (int i = 0; i < amount; ++i)
        {
            // Get enemy and set position:
            var goEnemy = ObjectPooler.Instance.GetPooledObject("Enemy");

            //Get the camera boundss
            var bounds = GameController.Instance.GetCameraBounds();

            var enemy = (Enemy)(goEnemy.GetComponent<Entity>());
            var rendererBounds = enemy.RendererBounds;
            float rendererSize = rendererBounds.size.y * 0.5f;

            //Set top and bottom corners modified by the size of the enemy render
            float yMin = bounds.min.y + rendererSize;
            float yMax = bounds.max.y - rendererSize;

            // Get a random yPos
            float y = Random.Range(yMin, yMax);

            // Spawn just off screen
            float x = bounds.max.x * spawnPositionOffset * Random.Range(randomXOffsetMin, randomXOffsetMax);

            enemy.SpawnEntity(new Vector2(x, y));

            GameController.Instance.AddListenerToEnemy(enemy);
        }
    }
}
