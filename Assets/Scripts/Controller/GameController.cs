﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    private static GameController instance;

    public static GameController Instance
    {
        get { return instance; }
    }

    public UIController uiController;
    public EnemyController enemyController;
    public PickupController pickupController;

    public Transform root;

    public enum GameState
    {
        MainMenu,
        Gameplay,
        GameOver,
    }

    private GameState gameState;

    public class OnGameStateChanged : UnityEvent<GameState>
    {
    }

    public OnGameStateChanged onGameStateChanged = new OnGameStateChanged();

    [SerializeField] private PlayerPawn playerPrefab;

    private PlayerPawn playerPawn;

    private Camera mainCamera;

    public struct CameraBounds
    {
        public Vector2 min;
        public Vector2 max;
    }
    private void Awake()
    {
        Initialise();
    }

    private void Initialise()
    {
        if (instance == null)
            instance = this;
  
        enemyController.Initialise();
        pickupController.Initialise();
        uiController.Initialise();
        // Must happen after uiController is initialised
        SetStateMainMenu();
    }

    #region SetGameStates
    public void SetStateMainMenu()
    {
        SetGameState(GameState.MainMenu);
    }

    public void SetStateGameplay()
    {
        SetGameState(GameState.Gameplay);
        SetupGame();
    }

    public class OnGameOver : UnityEvent<float>
    {
    }

    public OnGameOver onGameOver = new OnGameOver();

    public void SetStateGameOver()
    {
        SetGameState(GameState.GameOver);
    }

    private void SetGameState(GameState newGameState)
    {
        gameState = newGameState;

        //Send both events - highscore screen subscribes to onGameOver, but others don't.
        if (gameState == GameState.GameOver)
            onGameOver.Invoke(currentScore);
      
        onGameStateChanged.Invoke(gameState);

        Debug.Log("Transitioning to GameState " + gameState);

    }
    #endregion

    void SetupGame()
    {
        SetGameSettings();
        SpawnPlayer();
    }

    void SetGameSettings()
    {
        SetScore(0f);
        UpdatePlayerLives(playerLivesMax);
        UpdatePlayerWeaponAmmo();
    }

    void SpawnPlayer(bool gameStart = false)
    {
        // nullcheck covers first
        if (playerPawn == null)
        {
            playerPawn = Instantiate(playerPrefab, root, true);

            // Add listener for player death on new object
            playerPawn.onEntityDeath.AddListener(ReducePlayerLives);

            playerPawn.secondaryWeapon.onAmmoChanged.AddListener(
                (ammo) => UpdateAmmoCount(ammo));
        }

        // Return the player to the spawn location.
        playerPawn.SpawnEntity(root.position);
    }

    void UpdatePlayerWeaponAmmo()
    {
        playerPawn.ResetAmmo();
    }

    void Update()
    {
        if (playerPawn != null)
            UpdatePlayerInput();
    }

    void UpdatePlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SetStateMainMenu();

        if (gameState != GameState.Gameplay)
            return;

        InputPlayerMovement();
        InputPlayerShooting();
    }

    void InputPlayerMovement()
    {
        // Get raw input on x and y axis
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(x, y, 0f);
        direction.Normalize();

        // Get the camera bounds
        var bounds = GetCameraBounds();

        // Send to player pawns
        playerPawn.Move(direction, bounds.min, bounds.max);
    }

    void InputPlayerShooting()
    {
        //playerPawn.TryFirePrimaryWeapon();

        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0))
        {
            playerPawn.TryFirePrimaryWeapon();
        }

        if (Input.GetKey(KeyCode.V) || Input.GetKey(KeyCode.Mouse1))
        {
            playerPawn.TryFireSecondaryWeapon();
        }
    }

    // Reused in enemy controller
    public CameraBounds GetCameraBounds()
    {
        mainCamera = Camera.main;

        var bounds = new CameraBounds();

        // Find the bounds of the camera.
        // Min-Max return same values in a 3D camera. Figure out why if we want to add fancy graphics.
        bounds.min = mainCamera.ViewportToWorldPoint(new Vector2(0, 0));
        bounds.max = mainCamera.ViewportToWorldPoint(new Vector2(1, 1));

        return bounds;
    }


    #region PlayerLives

    [SerializeField] private int playerLivesMax;
    private int playerLivesCurrent;

    public class PlayerLivesChanged : UnityEvent<int>
    {
    }

    public PlayerLivesChanged onPlayerLivesChanged =
        new PlayerLivesChanged();


    public void UpdatePlayerLives(int playerLives)
    {
        playerLivesCurrent = playerLives;

        //Notify UI 
        if (onPlayerLivesChanged != null)
            onPlayerLivesChanged.Invoke(playerLivesCurrent);


        if (playerLivesCurrent <= 0)
        {
            SetStateGameOver();
        }
        else
        {
            SpawnPlayer();
        }
    }

    public void ReducePlayerLives()
    {
        int newLives = playerLivesCurrent - 1;
        UpdatePlayerLives(newLives);
    }

    #endregion

    #region Score

    float currentScore = 0f;

    public void SetScore(float score)
    {
        currentScore = score;
        onScoreChange.Invoke(currentScore);
    }

    public void AddListenerToEnemy(Enemy enemy)
    {
        //Clear any old listeners first.
        enemy.onEnemyDeath.RemoveAllListeners();
        enemy.onEnemyDeath.AddListener((scoreValue) => AddScore(scoreValue));
    }

    public class OnScoreChange : UnityEvent<float>
    {
    }

    public OnScoreChange onScoreChange = new OnScoreChange();

    public void AddScore(float scoreValue)
    {
        float newScore = currentScore + scoreValue;
        SetScore(newScore);
    }

    #endregion


    public class OnAmmoChangedHUD : UnityEvent<int>
    {
    }

    public OnAmmoChangedHUD onAmmoChangedHUD = new OnAmmoChangedHUD();

    void UpdateAmmoCount(int ammo)
    {
        onAmmoChangedHUD.Invoke(ammo);
    }
}
