﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    /*
    private static UIController instance;

    public static UIController Instance
    {
        get { return instance; }
    }
    */

    public UIWidget[] uiWidgets;

    public void Awake()
    {
        /*
        if (instance == null)
            instance = this;
        */
    }

    /// <summary>
    /// Initialised all UI elements.
    /// </summary>
    public void Initialise()
    {
        uiWidgets = GetComponentsInChildren<UIWidget>(true);

        for(int i = 0; i < uiWidgets.Length; ++i)
        {
            uiWidgets[i].Initialise();
        }
    }
}
