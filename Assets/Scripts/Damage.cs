﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generic component to be retrieved and checked for damage.
/// </summary>
public class Damage : MonoBehaviour
{
    public float dmgValue = 1f;
}
