﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 0.25f;

    [HideInInspector] public Vector3 direction = Vector2.right;

    public float lifeTime = 1f;
    float timeAlive = 0f;

    public LayerMask collisionMask;

    [SerializeField] private Renderer renderer;
    [SerializeField] private Collider2D collider;

    private void OnEnable()
    {
        timeAlive = 0f;
    }

    private void Update()
    {
        transform.position += direction * speed;

        timeAlive += Time.deltaTime;

        if (timeAlive >= lifeTime)
            Destroy();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Bullet don't have a health amount, if they hit something appropriate
        // they will always be destroyed. 
        if (collisionMask == (collisionMask | (1 << other.gameObject.layer)))
            Destroy();
    }

    private void Destroy()
    {
        // Can be reused by the pooler.
        gameObject.SetActive(false);
    }
}
