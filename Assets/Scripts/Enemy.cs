﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : Entity
{
    public float scoreValue = 10f;

    private void Update()
    {
        Vector3 direction = Vector3.left;
        direction.Normalize();

        transform.position += direction * MovementSpeed;
    }



    public OnEnemyDeath onEnemyDeath =
        new OnEnemyDeath();

    public class OnEnemyDeath : UnityEvent<float>
    {
    }

    public LayerMask outOfBoundsMask;

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        //Make sure enemies destroy themselves 
        if (outOfBoundsMask == (outOfBoundsMask | (1 << other.gameObject.layer)))
        {
            DisableEntity();
            return;
        }
    }

    public override void OnEntityDeath()
    {
        base.OnEntityDeath();

        onEnemyDeath.Invoke(scoreValue);

        //Prevents score from stacking up
        onEnemyDeath.RemoveAllListeners();
    }
}
