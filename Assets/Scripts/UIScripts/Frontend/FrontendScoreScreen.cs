﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using System.Linq; // Ugly for performance, but a timesaver in this situation

public class FrontendScoreScreen : UIWidget {

    [SerializeField] private TMP_InputField inputName;
    [SerializeField] private Button buttonMainMenu;
    [SerializeField] private Button buttonSubmitScore;

    [SerializeField] TextMeshProUGUI textCurrentName;
    [SerializeField] TextMeshProUGUI textCurrentScore;

    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private Transform scoreParent;

    public class Highscore
    {
        public string name;
        public float score;
        //public System.DateTime date;
    }

    // Scores
    List<Highscore> highscores = new List<Highscore>();
    List<FrontendHighscoreListElement> listElements = new List<FrontendHighscoreListElement>();

    float currentHighscore = 0f;

    public override void Setup()
    {
        var controller = GameController.Instance;

        // Technically can remove the gamestate from this event...
        controller.onGameOver.AddListener(
            (score) => OnGameOver(score));

        controller.onGameStateChanged.AddListener(
            (state) => OnGameStateChanged(state));

        // Buttons
        buttonMainMenu.onClick.AddListener(ButtonMainMenu);
        //buttonSubmitScore.onClick.AddListener(SubmitScore);

        // Input
        inputName.onEndEdit.AddListener((call) => AddScore());
        //inputName.onSubmit.AddListener((call) => AddScore());
    }

    public void OnGameOver(float score)
    {
        CacheeAndShowScore(score);
    }

    public void OnGameStateChanged(GameController.GameState currentGameState)
    {
        SetActive(currentGameState == GameController.GameState.GameOver);
        ResetScrollRect();
    }

    private void SetActive(bool active)
    {
        gameObject.SetActive(active);

        if (active)
        {
            inputName.gameObject.SetActive(true);
            textCurrentName.gameObject.SetActive(false);
            RefreshScores();
        }
    }

    private void CacheeAndShowScore(float score)
    {
        currentHighscore = score;
        textCurrentScore.text = score.ToString();
    }

    private void ResetScrollRect()
    {
        //scrollRect.verticalNormalizedPosition = 1f;
    }

    private void AddScore()
    {
        //Hide input field after submission and show current name
        inputName.gameObject.SetActive(false);
        textCurrentName.text = inputName.text;
        textCurrentName.gameObject.SetActive(true);

        var highscore = new Highscore();

        //For some reason TMPro Input doesn't include the string variable in it's event...
        highscore.name = inputName.text;
        highscore.score = currentHighscore;

        //Whoops
        highscores.Add(highscore);

        RefreshScores();
    }

    private void RefreshScores()
    {
        // Hide all objects in the list so they can be recycled; we don't want old scores
        // sticking around.
        foreach(FrontendHighscoreListElement element in listElements)
        {
            element.gameObject.SetActive(false);
        }

        //Sort list by highest score.
        //Should actually add date here as a second sorting parameter; but... 
        highscores.OrderBy(score => score.score);

        for(int i = 0; i < highscores.Count; ++i)
        {
            var highscore = highscores[i];

            var goHighscore = ObjectPooler.Instance.GetPooledObject("HighscoreListElement");
            var template = goHighscore.GetComponent<FrontendHighscoreListElement>();

            template.SetInfo(highscore.name, highscore.score.ToString());

            goHighscore.transform.SetParent(scoreParent);
            goHighscore.transform.SetAsFirstSibling();

            // Correcting for weird distorions
            Vector3 correctedPos = goHighscore.transform.position;
            correctedPos.z = 0f;

            goHighscore.transform.position = correctedPos;
            goHighscore.transform.localScale = new Vector3(1f, 1f, 1f);

            goHighscore.SetActive(true);

            listElements.Add(template);
        }
    }

    private void ButtonMainMenu()
    {
        GameController.Instance.SetStateMainMenu();
    }
}
