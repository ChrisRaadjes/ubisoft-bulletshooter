﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class FrontendMainMenu : UIWidget {

    [SerializeField] private Button buttonStartGame;
    [SerializeField] private Button buttonQuit;

    public override void Setup()
    {
        base.Setup();

        var controller = GameController.Instance;
        controller.onGameStateChanged.AddListener(
            (currentGameState) => OnGameStateChanged(currentGameState));

        buttonStartGame.onClick.AddListener(StartGame);
        buttonQuit.onClick.AddListener(Quit);
    }


    public void OnGameStateChanged(GameController.GameState currentGameState)
    {
        bool showMenu = (currentGameState == GameController.GameState.MainMenu);
        SetActive(showMenu);
    }

    private void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    public void StartGame()
    {
        GameController.Instance.SetStateGameplay();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
