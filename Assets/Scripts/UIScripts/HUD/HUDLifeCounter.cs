﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDLifeCounter: UIWidget
{
    public TextSetter textLifeCount;

    public override void Setup()
    {
        base.Setup();

        var controller = GameController.Instance;

        controller.onGameStateChanged.AddListener(
            (gameState) => OnGameStateChanged(gameState));


        controller.onPlayerLivesChanged.AddListener(
            (life) => UpdatePlayerLivesHUD(life));
    }

    public void OnGameStateChanged(GameController.GameState gameState)
    {
        SetActive(gameState == GameController.GameState.Gameplay);
    }

    public void SetActive(bool show)
    {
        gameObject.SetActive(show);
    }

    public void UpdatePlayerLivesHUD(int lifeCount)
    {
        textLifeCount.text = lifeCount.ToString();
    }
}
