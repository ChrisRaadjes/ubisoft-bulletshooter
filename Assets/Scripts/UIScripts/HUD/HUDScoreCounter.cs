﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDScoreCounter: UIWidget
{
    public TextSetter textScoreAmount;

    public override void Setup()
    {


        var controller = GameController.Instance;

        controller.onGameStateChanged.AddListener(
            (gameState) => OnGameStateChanged(gameState));

        controller.onScoreChange.AddListener(
            (scoreAmount) => UpdatePlayerLivesHUD(scoreAmount));
    }

    public void OnGameStateChanged(GameController.GameState gameState)
    {
        SetActive(gameState == GameController.GameState.Gameplay);
    }

    public void SetActive(bool show)
    {
        gameObject.SetActive(show);
    }

    public void UpdatePlayerLivesHUD(float scoreAmount)
    {
        textScoreAmount.text = scoreAmount.ToString();
    }
}
