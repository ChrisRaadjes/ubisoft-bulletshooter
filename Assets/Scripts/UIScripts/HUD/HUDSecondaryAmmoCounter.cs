﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDSecondaryAmmoCounter : UIWidget
{
    public TextSetter textAmmoCount;

    public override void Setup()
    {
        base.Setup();

        var controller = GameController.Instance;

        controller.onGameStateChanged.AddListener(
            (gameState) => OnGameStateChanged(gameState));

        controller.onAmmoChangedHUD.AddListener(
            (ammo) => UpdateAmmoCount(ammo));

    }

    public void OnGameStateChanged(GameController.GameState gameState)
    {
        SetActive(gameState == GameController.GameState.Gameplay);
    }

    public void SetActive(bool show)
    {
        gameObject.SetActive(show);
    }

    public void UpdateAmmoCount(int ammoCount)
    {
        textAmmoCount.text = ammoCount.ToString();
    }
}
