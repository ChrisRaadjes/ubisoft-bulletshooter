﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all UI components. Communicate with an eventual 
/// UI controller, if needed. 
/// </summary>
public class UIWidget: MonoBehaviour
{
    public virtual void Initialise()
    {
        Setup();
    }

    public virtual void Setup()
    {
    }
}
