﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Convenience class that allows setting text on TMPRo, without having to include
/// the TMPro header in every UIWidget.
/// </summary>
public class TextSetter : MonoBehaviour
{
    private TextMeshProUGUI textComponent;

    bool initialised;

    void Awake ()
    {
        if (!initialised) Initialise();
	}

    void Initialise()
    {
        initialised = true;
        textComponent = GetComponent<TextMeshProUGUI>();
    }

    // Lowercase "t" because the user shouldn't know they're going through a helper component
    // (Unity UGUI standards)
    public string text
    {
        get
        {
            if (textComponent != null)
            {
                return textComponent.text;
            }
            else
                return string.Empty;
        }
        set
        {
            if(textComponent != null)
                textComponent.text = value;
        }
    }


}
