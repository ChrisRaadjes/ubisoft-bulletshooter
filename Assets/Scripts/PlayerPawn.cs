﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Entity
{
    //Temp variable
    public float fireRate = 0.1f;

    public void Move(Vector3 direction, Vector3 cameraBoundsMin, Vector3 cameraBoundsMax)
    {
        //transform.position += direction * MovementSpeed;

        Vector3 newPos = transform.position;
        newPos += direction * MovementSpeed;

        // Adjust the camera bounds by the size of the player renderer
        Bounds bounds = RendererBounds;

        float rendererXSize = bounds.size.x * 0.5f;
        float rendererYSize = bounds.size.y * 0.5f;

        cameraBoundsMin.x += rendererXSize;
        cameraBoundsMax.x -= rendererXSize;

        cameraBoundsMin.y += rendererYSize;
        cameraBoundsMax.y -= rendererYSize;

        // Clamp to bounds of camera

        newPos.x = Mathf.Clamp(newPos.x, cameraBoundsMin.x, cameraBoundsMax.x);
        newPos.y = Mathf.Clamp(newPos.y, cameraBoundsMin.y, cameraBoundsMax.y);

        // Set position
        transform.position = newPos;
    }

    // Setup basic shooting for now - we'll add the weapon system after pooling is done.
    // This feels clunky as a solution.
    public void TryFirePrimaryWeapon()
    {
        if(primaryWeapon != null)
            primaryWeapon.TryFireWeapon();
    }

    public void TryFireSecondaryWeapon()
    {
        if(secondaryWeapon != null)
            secondaryWeapon.TryFireWeapon();
    }

    #region Events

    public void ResetAmmo()
    {
        primaryWeapon.ResetAmmo();
        secondaryWeapon.ResetAmmo();
    }


    [SerializeField] private LayerMask pickupMask;

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        // Check for pickup collisions
        if(pickupMask == (pickupMask | (1 << other.gameObject.layer)))
        {
            var pickup = other.gameObject.GetComponent<Pickup>();

            if(pickup != null)
            {
                secondaryWeapon.UpdateAmmo(pickup.AmmoCount);
            }
        }
    }

    #endregion

}
