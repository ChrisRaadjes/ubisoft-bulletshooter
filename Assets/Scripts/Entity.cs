﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Base class for both player and enemy ships
/// </summary>
public abstract class Entity : MonoBehaviour
{
    [SerializeField] private float maxHealth;
    private float currentHealth;
    [SerializeField] private float movementSpeed;

    // Weapons
    public Weapon primaryWeapon;
    public Weapon secondaryWeapon;

    [SerializeField] private Renderer renderer;
    [SerializeField] private Collider2D collider;

    // Event
    public UnityEvent onEntityDeath;

    public float MovementSpeed
    {
        get { return movementSpeed; }
    }

    public Bounds RendererBounds
    {
        get { return renderer.bounds; }
    }

    public void SpawnEntity(Vector3 worldPos)
    {
        //SetColliderState(true);
        gameObject.SetActive(true);
        SetColliderIsTrigger(true);
        ResetEntityHealth();
        MoveToLocation(worldPos);
    }

    void ResetEntityHealth()
    {
        currentHealth = maxHealth;
    }

    void SetColliderState(bool active)
    {
        renderer.enabled = active;
        collider.enabled = active;
    }


    public void MoveToLocation(Vector3 worldPos)
    {
        transform.position = worldPos;
    }


    [SerializeField] private LayerMask damageMask;

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        // Damage is applied if the colliding object matches one of the layer in damageMask. 
        // Should give a lot of design control. 

        if (damageMask == (damageMask | (1 << other.gameObject.layer)))
        {
           var damage = other.gameObject.GetComponent<Damage>();

            if(damage != null)
            {
                ApplyDamage(damage.dmgValue);

                //Break functions
                return;
            }
        }
    }

    // This and apply damage could be the same function if I refactor some stuf...
    public void SetHealth(float health)
    {
        currentHealth = health;
    }

    public void ApplyDamage(float damage)
    {
        // Apply damage value
        currentHealth -= damage;
        
        if(currentHealth <= 0)
        {
            OnEntityDeath();
        }
    }

    #region Events

    public virtual void OnEntityDeath()
    {
        DisableEntity();
        // Notify any registered GameObjects that the entity has died. 
        onEntityDeath.Invoke();
    }

    public void DisableEntity()
    {
        gameObject.SetActive(false);
        SetColliderIsTrigger(false);
    }

    void SetColliderIsTrigger(bool value)
    {
        collider.isTrigger = value;
    }

    #endregion;
}
 